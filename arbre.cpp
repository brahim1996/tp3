
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct noeud {
 int valeur;
 noeud *gauche;
 noeud *droit;
};

void affiche_espaces(int d){

for (int i(0);i<d;i++)
cout<<" ";
}


int maxi(int a, int b){
if (a<b)
return b;
return a;

}
class ABR {
private:
 noeud *racine; // La racine de l’arbre binaire de recherche.
public:
ABR (noeud *r)
{racine=new noeud(*r);
};
noeud* & getRacine(){return racine;}

int hauteur(noeud* racine){
noeud* courant=racine;
if (! courant)
return 0;
else {
int hg=hauteur(courant->gauche);
int hd=hauteur(courant->droit);
return (1+maxi(hg,hd));
}
}
void inserer(int d){
noeud* nouveau=new noeud;
nouveau->valeur=d;
nouveau->gauche=0;
nouveau->droit=0;
if (racine==NULL){
racine=nouveau;
return;
}
noeud* courant=racine;
//noeud* sauv=racine;
noeud* pere=NULL;

while (courant){
pere=courant;
if (courant->valeur==d)
return;
else if (courant->valeur<d)
courant=courant->droit;
else
courant=courant->gauche;
}

 if (pere->valeur<d)
pere->droit=nouveau;
else
pere->gauche=nouveau;
};

void supp (noeud* racine) {
if (racine->gauche)
supp(racine->gauche);
delete(racine);
if (racine->droit)
supp(racine->droit);
}
~ABR(){
supp(racine);
}
void affiche(noeud* racine){
//eud* courant=racine;

if (racine->gauche)
affiche(racine->gauche);


if (racine->droit)
affiche(racine->droit);

cout<<racine->valeur<<endl;
}
void ascendant(noeud* racine,int d)
{
if (recherche(racine,d)){
noeud* courant=racine;
while (courant){

if (courant->valeur==d)
return;
else if (courant->valeur<d){
cout<<courant->valeur<<endl;
courant=courant->droit;}
else
{cout<<courant->valeur<<endl;
courant=courant->gauche;
}}}
else cout<<"elt nexite pas"<<endl;
}


noeud* recherche(noeud* racine,int d){
noeud* courant=racine;
while (courant){
if (courant->valeur==d){
return courant;
}
if (courant->droit && courant->valeur<d)
{courant=courant->droit;}
else if
(courant->gauche && courant->valeur>d)
{courant=courant->gauche;}
else {  return 0;}

}}

void afficheDescendant(noeud* racine, int d){
noeud* node=recherche(racine,d);
if (node){
if (node->droit)
aff(node->droit);
if (node->gauche)
aff(node->gauche);
}
else cout<<"elt non exist"<<endl;
//cout<<"nexite pas;


}

void aff(noeud* racine){
int niveau=0;
int longeur=hauteur(racine);
vector <vector<noeud*>> matrice;
vector<noeud*> ligne1;
ligne1.push_back(new noeud(*racine));
matrice.push_back(ligne1);

while(niveau<longeur){
niveau++;
vector<noeud*> ligne;
matrice.push_back(ligne );
for (int i(0);i<matrice[niveau-1].size();i++){

if((matrice[niveau-1])[i]->droit){
noeud* n= new noeud (*((matrice[niveau-1])[i]->droit));
matrice[niveau].push_back(n);}
if((matrice[niveau-1])[i]->gauche){
noeud* n= new noeud (*((matrice[niveau-1])[i]->gauche));
matrice[niveau].push_back(n);}

}

}


for (int i(matrice.size()-1);i>-1;i--){
for (int j(matrice[i].size()-1);j>-1;j--)
{cout<<((matrice[i])[j])->valeur;affiche_espaces(2);}
cout<<endl;
}
for (int i(matrice.size()-1);i>-1;i--){
for (int j(matrice[i].size()-1);j>-1;j--)
delete ((matrice[i])[j]);
}}
noeud* SupprimerMin ( noeud *& r )
{
 //assert ( r != NULL ) ; // on vérifie que l'arbre n'est pas vide
 if ( r->gauche != NULL ) // on continue à gauche
 return SupprimerMin ( r->gauche ) ;
 else // le minimum est trouvé à ce stade
 {
 noeud *temp = r ;
 r = r->droit ;
 return temp ;
 }
}



void AideSupprimer ( noeud * &r, int val )
{
 if ( r == NULL )
 cout << val << "n'est pas dans l'arbre \n";
 else
 if ( val < r->valeur) // chercher à gauche
 AideSupprimer ( r->gauche, val ) ;
 else
 if ( val > r->valeur ) // chercher à droite
 AideSupprimer ( r->droit, val ) ;
 else
 {
 noeud *temp = r ;
 if ( r->gauche == NULL ) // a seulement un fils droit
 r = r->droit ; // faire pointer vers la droite
 else
 if ( r->droit == NULL ) // seulement un fils gauche
 r = r->gauche ;
 else
 {
 // le nœud a deux fils
 temp = SupprimerMin ( r->droit ) ;
// supprime le plus petit du fils droit
 temp->valeur=val;
 }
 delete temp ;
 }
}
// supprime tous l

};



int main()
{
noeud n;
n.valeur=9;
n.gauche=0;
n.droit=0;

ABR arbre(&n);
cout<<"Ce programme effectue des opérations sur un arbre binaire de recherche à partir d'un fichier texte intitulé entree.txt"<<endl<<endl;
ifstream fichier("entree.txt");
string ligne;
int i=1;
while (getline(fichier,ligne))

{

if (ligne=="A")
{
cout<<"Opération "<<i<<": Affichge de l'arbre"<<endl;
arbre.aff(arbre.getRacine());
}

else if (ligne=="H"){

cout<<"Opération "<<i<<": Affichage  de la hauteur de l'arbre"<<endl;
cout<<arbre.hauteur(arbre.getRacine())<<endl<<endl;
}

else if (ligne[0]=='I'){
cout<<"Opération "<<i<<": Insertion d'un élement"<<endl<<endl;
string nbr="";
for (int i(2);i<ligne.size();i++)
nbr+=ligne[i];
arbre.inserer(stoi(nbr));
}
else if (ligne[0]=='S'){
string nbr="";
for (int i(2);i<ligne.size();i++)
nbr+=ligne[i];
cout<<"Opération "<<i<<": Suppresion du neoud de valeur "<<nbr<<endl<<endl;
arbre.AideSupprimer(arbre.getRacine(),stoi(nbr));
}
else if (ligne[0]=='G'){
string nbr="";
for (int i(2);i<ligne.size();i++)
nbr+=ligne[i];
cout<<"Opération "<<i<<": Affichage des ascendants du noeud de valeur "<<stoi(nbr)<<endl<<endl;
arbre.ascendant(arbre.getRacine(),stoi(nbr));
}
else if (ligne[0]=='D'){
string nbr="";
for (int i(2);i<ligne.size();i++)
nbr+=ligne[i];
cout<<"Opération "<<i<<": Affichage des descandants du noeud de valeur "<<stoi(nbr)<<endl<<endl;
arbre.afficheDescendant(arbre.getRacine(),stoi(nbr));
}
else cout<<"La ligne numero "<<i<<" du fichier entree.txt ne correspond à aucune opération !"<<endl;
i++;
}

return 0;
}
